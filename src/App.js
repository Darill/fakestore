import { Route, Routes } from "react-router-dom";
import { AddProduct } from "./page/addProduct";
import { EditProduct } from "./page/editProduct";
import { ListProduct } from "./page/listProduct";

function App() {
  return (
    <Routes>
      <Route path="/" element={<ListProduct />} />
      <Route path="addproduct" element={<AddProduct />} />
      <Route path="editproduct/:id" element={<EditProduct />} />
    </Routes>
  );
}

export default App;
