import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
    body{
        margin: 0px;
        padding: 0px;
        font-family: 'Poppins', sans-serif;
    }
`

export default GlobalStyle;