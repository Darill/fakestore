import { createAsyncThunk, createEntityAdapter, createSlice } from "@reduxjs/toolkit";
import axios from "axios";


export const getProduct = createAsyncThunk("product/getProduct", async () => {
    const res = await axios.get("http://localhost:8000/products")
    return res.data;
})

export const addProduct = createAsyncThunk("product/addProduct", async ({ title, price }) => {
    const res = await axios.post("http://localhost:8000/products", {
        title,
        price
    })
    return res.data;
})
export const delProduct = createAsyncThunk("product/delProduct", async (id) => {
    await axios.delete(`http://localhost:8000/products/${id}`)
    return id;
})
export const updateProduct = createAsyncThunk("product/updateProduct", async ({ id, title, price }) => {
    const res = await axios.patch(`http://localhost:8000/products/${id}`, {
        title,
        price
    })
    return res.data;
})

const productEntity = createEntityAdapter({
    selectId: (product) => product.id
})

const productReducer = createSlice({
    name: 'product',
    initialState: productEntity.getInitialState(),
    extraReducers: {
        [getProduct.fulfilled]: (state, action) => {
            productEntity.setAll(state, action.payload)
        },
        [addProduct.fulfilled]: (state, action) => {
            productEntity.addOne(state, action.payload)
        },
        [delProduct.fulfilled]: (state, action) => {
            productEntity.removeOne(state, action.payload)
        },
        [updateProduct.fulfilled]: (state, action) => {
            productEntity.updateOne(state, { id: action.payload.id, updates: action.payload })
        },
    }

})

export const productSelector = productEntity.getSelectors(state => state.product);
export default productReducer.reducer