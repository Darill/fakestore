import React from 'react'
import { Delete, Edit, Buttons } from '../styled/button'
import { Text } from '../styled/text'

export const Button = (props) => {
    return (
        <Buttons>
            <Delete>
                <Text>{props.delete}</Text>
            </Delete>
            <Edit>
                <Text>{props.edit}</Text>
            </Edit>
        </Buttons>
    )
}
