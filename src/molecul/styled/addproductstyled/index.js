import styled from "styled-components";

export const Container = styled.div`
    display: flex;
    width: 100%;
    height: 100vh;
    align-items: center;
    flex-direction: column;
    overflow: hidden;
`
export const Card = styled.form`
    background-color: #fff;
    padding: 20px;
    border-radius: 8px;
    display: flex;
    flex-direction: column;
    box-shadow: rgba(100, 100, 111, 0.2) 0px 7px 29px 0px;
    width: 20%;
    @media only screen and (max-width: 600px) {
        width: 80%;
    }
`
export const Input = styled.input`
    border-radius: 4px;
    padding: 5px;
    font-size: 16px;
    margin: 20px 0px;
`