import styled from "styled-components";

export const Table = styled.table`
    text-align: center;
    width: 40%;
    border-collapse: collapse;
    @media only screen and (max-width: 600px) {
        width: 90%;
    }
`
export const TdButton = styled.td`
    display: flex;
    justify-content: center;
`
export const SuccessButton = {
    textDecoration: 'none',
    color: '#fff',
    backgroundColor: '#b7e951',
    padding: "3px 5px",
    border: "0px",
    borderRadius: "3px",
    margin: 10,
    cursor: 'pointer',
}
export const DeleteButton = {
    color: '#fff',
    backgroundColor: '#ff2a2a',
    padding: "3px 5px",
    border: "0px",
    borderRadius: "3px",
    margin: 10,
    cursor: 'pointer'
}
