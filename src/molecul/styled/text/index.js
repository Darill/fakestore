import styled from "styled-components";

export const Text = styled.p`
    margin: 0;
    padding: 0;
    font-size: ${props => props.sz || "14px"};
    color: ${props => props.c || "#fff"};
`