import React, { useState } from 'react'
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { addProduct } from '../../feature/reducers';
import { Card, Container, Input } from '../../molecul/styled/addproductstyled';
import { SuccessButton } from '../../molecul/styled/table';
import { Text } from '../../molecul/styled/text';

export const AddProduct = () => {
    const [title, setTitle] = useState('')
    const [price, setPrice] = useState('')
    const navigate = useNavigate()
    const dispatch = useDispatch()

    const handleSubmit = async(e) => {
        e.preventDefault()
        await dispatch(addProduct({ title, price }))
        navigate('/')
    }

    return (
        <Container>
            <Card onSubmit={handleSubmit}>
                <Text c="#000" sz="16px">Nama Product</Text>
                <Input type="text" value={title} placeholder="Masukan Product" onChange={(e) => setTitle(e.target.value)}/>
                <Text c="#000" sz="16px">Harga Product</Text>
                <Input type="number" value={price} placeholder="Masukan Harga" onChange={(e) => setPrice(e.target.value)}/>
                <button style={SuccessButton} ><Text sz="16px">Add Product</Text></button>
            </Card>
        </Container>
    )
}
