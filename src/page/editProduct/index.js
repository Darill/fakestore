import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';
import { getProduct, productSelector, updateProduct } from '../../feature/reducers';
import { Card, Container, Input } from '../../molecul/styled/addproductstyled';
import { SuccessButton } from '../../molecul/styled/table';
import { Text } from '../../molecul/styled/text';

export const EditProduct = () => {
  const [title, setTitle] = useState('')
  const [price, setPrice] = useState('')
  const navigate = useNavigate()
  const dispatch = useDispatch()
  const { id } = useParams()
  const product = useSelector((state) => productSelector.selectById(state, id))

  useEffect(() => {
    dispatch(getProduct())
  }, [dispatch])

  useEffect(() => {
    if (product) {
      setTitle(product.title)
      setPrice(product.price)
    }
  }, [dispatch])

  const handleUpate = async (e) => {
    e.preventDefault()
    await dispatch(updateProduct({ id, title, price }));
    navigate('/')
  }

  return (
    <Container>
      <Card onSubmit={handleUpate}>
        <Text c="#000" sz="16px">Nama Product</Text>
        <Input type="text" value={title} placeholder="Masukan Product" onChange={(e) => setTitle(e.target.value)} />
        <Text c="#000" sz="16px">Harga Product</Text>
        <Input type="number" value={price} placeholder="Masukan Harga" onChange={(e) => setPrice(e.target.value)} />
        <button style={SuccessButton} ><Text sz="16px">Add Product</Text></button>
      </Card>
    </Container>
  )
}
