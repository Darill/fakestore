import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { getProduct, productSelector, delProduct } from '../../feature/reducers';
import { SuccessButton, DeleteButton, Table, TdButton } from '../../molecul/styled/table';
import { Link } from 'react-router-dom';
import {Text} from '../../molecul/styled/text'
import ConvertToRupiah from '../../feature/convertToRp';
import {Container} from '../../molecul/styled/addproductstyled'

export const ListProduct = () => {
    const dispatch = useDispatch()
    const products = useSelector(productSelector.selectAll)

    useEffect(() => {
        dispatch(getProduct())
    }, [dispatch]);

    return (
        <Container>
            <Table>
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {products.map((product, index) => (
                        <tr key={index} style={{borderTop: "0.5px solid black"}}>
                            <td>{index + 1}.</td>
                            <td>{product.title}</td>
                            <td><ConvertToRupiah props={product.price} /></td>
                            <TdButton>
                                <button onClick={() => dispatch(delProduct(product.id))} style={DeleteButton}>Delete</button>
                                <Link to={`/editproduct/${product.id}`} style={SuccessButton}><Text>Edit</Text></Link>
                            </TdButton>
                        </tr>
                    ))}
                </tbody>
            </Table>
            <Link to='/addproduct' style={SuccessButton}>Add Product</Link>
        </Container>
    )
}
